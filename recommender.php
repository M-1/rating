<?php

/**
 * Description of dummy_recommender
 * PS: třída se nezabývá kontrolou dat - při chybě spadne celý program (což ale u experimentů tohoto druhu není žádná tragedie)
 * @author peska
 */
class Recommender {

	private $train_set;
	private $test_set_fname;
	private $solution_fname;
	private $objects;
	private $users = array();

	/**
	 * Konstruktor třídy: nejprve se naučí model UP a následně provede predikci pro všechny páry uživatel-objekt z test-setu
	 * @param type $input_train_set_fname název souboru obsahujícího train set
	 * @param type $input_test_set název souboru obsahujícího test set
	 * @param type $output_solution název souboru kam se má uložit výsledné řešení
	 * @param type $objects_fname název souboru obsahujícího atributy objektu
	 */
	function __construct($input_train_set_fname, $input_test_set, $output_solution, $objects_fname) {
		$this->train_set = new Training($input_train_set_fname, FALSE, TRUE);
		$this->test_set_fname = $input_test_set;
		$this->solution_fname = $output_solution;
		$this->objects = new Objects($objects_fname, TRUE);

		//var_dump($this->objects->Get(21164));
		//var_dump($this->train_set->Get(33));


		$this->learn_user_model();
		$this->evaluate();
	}

	/**
	 * Tato dummy funkce pouze načte train_set (atributy objektu nepoužívá) a uloží si baseline predictors
	 * Vaše řešení může navíc např. zpracovávat atributy objektů, nebo rozumněji definovat model preferencí
	 */
	function learn_user_model() {
		foreach ($this->train_set->getData() as $user_id => $user_ratings) {
			$this->users[$user_id] = new User($user_ratings, $this->objects);
		}

		//var_dump($this->users[1]);

		/* $object_rating_sum = array();
		  $object_rating_count = array();
		  $user_rating_sum = array();
		  $user_rating_count = array();
		  $rating_sum_total = 0;
		  $rating_count_total = 0;

		  $handle = fopen($this->train_set, "r");
		  while (($buffer = fgets($handle)) !== false) {
		  //$row[0] = uid, $row[1] = oid, $row[2] = rating
		  $row = explode(",", $buffer);
		  $user_rating_sum[intval($row[0])] += $row[2];
		  $user_rating_count[intval($row[0])] ++;
		  $object_rating_sum[intval($row[1])] += $row[2];
		  $object_rating_count[intval($row[1])] ++;
		  $rating_sum_total += $row[2];
		  $rating_count_total++;
		  }
		  //zpracujeme ratingy do baseline predictors
		  $this->average_rating = $rating_sum_total / $rating_count_total;

		  foreach ($user_rating_count as $key => $rating_count) {
		  if ($rating_count > 0) {
		  $rating_sum = $user_rating_sum[$key];
		  $this->user_factors[$key] = ($rating_sum / $rating_count) - $this->average_rating;
		  }
		  }
		  foreach ($object_rating_count as $key => $rating_count) {
		  if ($rating_count > 0) {
		  $rating_sum = $object_rating_sum[$key];
		  $this->object_factors[$key] = ($rating_sum / $rating_count) - $this->average_rating;
		  }
		  }
		  echo "Train set processed. Total instances: " . $rating_count_total . "\n";

		 */
	}

	/**
	 * Metoda evaluate pro každý řádek z test_set spočítá sumu baseline prediktorů jako předopkládané hodnocení a to uloží do solution
	 */
	function evaluate() {
		$handle = fopen($this->test_set_fname, 'r');
		$output = fopen($this->solution_fname, 'w');
		$count = 0;
		while (($buffer = fgets($handle)) !== false) {
			$count ++;
			//$row[0] = uid, $row[1] = oid
			$row = explode(",", trim($buffer));
			$user_id = $row[0];
			$object_id = $row[1];
			$rating = $this->users[$user_id]->Rate($object_id, $this->objects);
			fwrite($output, "" . intval($row[0]) . "," . intval($row[1]) . "," . $rating . "\n");
		}
		echo "Test set processed. Total instances: " . $count . "\n";
	}

}

/**
 * Přečte csv a poskytuje přístup k jednotlivým řádkům/polím
 */
abstract class Data {

	protected $data = array();
	protected $column_names = array();

	public function __construct($file, $first_line_contains_names = FALSE, $multiple_lines_sharing_id = FALSE) {

		$first = $first_line_contains_names;

		$handle = fopen($file, 'r');
		while (($buffer = fgets($handle)) !== false) {
			// $row[0] = id, $row[1] = vyrobce, $row[2] = model
			$row = explode(',', trim($buffer));

			$id = $row[0];
			array_shift($row); // Remove unnecessary id

			if ($first) {
				$this->column_names = $row;
				$first = FALSE;
				continue;
			}

			/* // pro items.csv
			  $object = array(
			  'vyrobce' => $row[1],
			  'model' => $row[2],
			  'rok_vyroby' => $row[3],
			  'najete_km' => $row[4],
			  'objem' => $row[5],
			  'vykon' => $row[6],
			  'palivo' => $row[7],
			  'spotreba' => $row[8],
			  'cena' => $row[9]
			  ); */



			if ($multiple_lines_sharing_id) {
				if (isset($this->data[$id])) {
					// Add to existing array
					$this->data[$id][] = $row;
				} else {
					// Create new array
					$this->data[$id] = array($row);
				}
			} else {
				$this->data[$id] = $row; // Save it
			}
		}
	}

	public function Get($id) {
		if (!isset($this->data[$id])) {
			throw new Exception('non-existant id');
		}
		return $this->data[$id];
	}

	public function getColumnNames() {
		return $this->column_names;
	}

	public function getData() {
		return $this->data;
	}

}

class Objects extends Data {
	
}

class Training extends Data {
	
}

class NumberSet {

	protected $data = array();
	protected $finalized = FALSE;

	public function add($value) {
		$this->data[] = $value;
	}

	public function finalize() {
		if (!$this->finalized) {
			sort($this->data);
			$this->finalized = TRUE;
		}
	}

	protected function getIndex($position) {
		return (int) ($position * (count($this->data) - 1));
	}

	/*
	  public static function getMedian() {
	  return $this->getPercentile(0.5);
	  }
	 */

	/**
	 * 
	 * @param float $value in interval [0, 1]
	 * @return type
	 */
	public function getPercentile($value) {
		$this->finalize();
		return $this->data[$this->getIndex($value)];
	}

	/**
	 * 
	 * @author thearbitcouncil at gmail dot com
	 * @link http://php.net/manual/en/ref.math.php#48783 source
	 * @param type $arr
	 * @return int
	 */
	function getAverage() {
		if (!count($this->data)) {
			return 0;
		}

		$sum = 0;
		for ($i = 0; $i < count($this->data); $i++) {
			$sum += $this->data[$i];
		}

		return $sum / count($this->data);
	}

	/**
	 * 
	 * @author thearbitcouncil at gmail dot com
	 * @link http://php.net/manual/en/ref.math.php#48783 source
	 * @param type $arr
	 * @return int
	 */
	public function getVariance() {
		if (!count($this->data)) {
			return 0;
		}

		$mean = $this->getAverage();

		$sos = 0; // Sum of squares
		for ($i = 0; $i < count($this->data); $i++) {
			$sos += ($this->data[$i] - $mean) * ($this->data[$i] - $mean);
		}

		return $sos / (count($this->data) - 1);  // denominator = n-1; i.e. estimating based on sample 
		// n-1 is also what MS Excel takes by default in the VAR function
	}

}

interface Preference {

	/**
	 * @param string $name name of an attribute this preference describes
	 * @param array $training_data [i] => {[0] => value, [1] => rating}
	 */
	function __construct(array $training_data);

	function getRating($value);

	function getWeight();
}

/**
 * Trojúhelníková preference
 * maximum v ideálním bodě
 * minimum v bodech vzdálenějších $diameter a více od $ideal_point
 */
class NumericPreference implements Preference {

	protected $ideal_point;
	protected $left_min;
	protected $right_min;
	protected $weight;
	protected $min_rating = 1; // intentionally inverse - to be overridden by any real rating
	protected $max_rating = 0;

	function __construct(array $training_data) {
		//var_dump('D ', $training_data);
		$values = new NumberSet();
		$ratings = new NumberSet();

		/* $lines = count($training_data);
		  $min_value = 0;
		  $max_value = 0; */

		foreach ($training_data as $line) {

			$value = $line[0];
			$rating = $line[1];

			$values->add($value);
			$ratings->add($rating);

			if ($rating > $this->max_rating) {
				$this->max_rating = $rating;
				$this->ideal_point = $value;
			}

			if ($rating < $this->min_rating) {
				$this->min_rating = $rating;
			}
		}

		$epsilon = 0.00;

		$this->left_min = $values->getPercentile($epsilon);
		$this->right_min = $values->getPercentile(1 - $epsilon);
		// TODO: zkontrolovat jestli left_min < ideal < right_min
		$this->weight = 1 - pow($ratings->getVariance(), 0.5); // std dev.
	}

	public function getRating($value) {
		// Count the value
		if ($value == $this->ideal_point) {
			return 1;
		} else if ($value < $this->ideal_point) {
			if ($value < $this->left_min) {
				return 0;
			}
			// The left side of the triangle
			// d="left diameter"
			// m="middle"="ideal point"
			// f(x)=x*a+b
			// a=1/d
			// b=-(m/d)+1
			$left_d = $this->ideal_point - $this->left_min; // d

			$left_half = $value * (1 / $left_d) - ($this->ideal_point / $left_d) + 1;
			$res = $left_half;
		} else {
			if ($value > $this->right_min) {
				return 0;
			}
			// f(x)=x*a+b
			// a=-1/d
			// b=m/d+1
			$right_d = $this->right_min - $this->ideal_point; // d
			$right_half = $value * (- 1 / $right_d) + ($this->ideal_point / $right_d) + 1;
			$res = $right_half;
			/* if($print) {
			  var_dump($res);
			  } */
		}

		//$res = min(array($left_half, $right_half));

		if ($res > 1.01) {
			throw new Exception($res . ' > 1: ' . print_r(array($left_d, $left_half, $right_d, $right_half, $this), TRUE));
		}

		$res = max(array($res, 0));

		// BIAS - scale and shift the the computed value ([1, 0] -> [min_seen_rating, max_seen_rating])
		$res = $this->min_rating + ($this->max_rating - $this->min_rating) * $res;

		return $res;
	}

	public function getWeight() {
		return $this->weight;
	}

}

class StringPreference implements Preference {

	protected $prefered_values = array();

	public function __construct(array $training_data) {
		// $values['value'] = {[0] => rating sum, [1] => count of ratings}
		$values = array();

		foreach ($training_data as $line) {
			$value = $line[0];
			$rating = $line[1];

			if (!isset($values[$value])) {
				$values[$value] = array($rating, 1);
			} else {
				$values[$value][0] += $rating;
				$values[$value][1] ++;
			}
		}

		$avg_sum = 0;
		$avg_count = 0;

		foreach ($values as $value => $rating_stats) {
			$avg = $rating_stats[0] / $rating_stats[1];
			$avg_sum += $avg;
			$avg_count ++;
		}

		$avg_avg = $avg_sum / $avg_count; // average of average ratings of all values of this attribute

		foreach ($values as $value => $rating_stats) {
			$avg_rating = $rating_stats[0] / $rating_stats[1];

			if ($avg_rating > $avg_avg) {
				$this->prefered_values[] = $value;
			}
		}
	}

	public function getRating($value) {
		if (in_array($value, $this->prefered_values)) {
			return 1;
		} else {
			return 0;
		}
	}

	public function getWeight() {
		return 0.5; // TODO!
	}

}

class User {

	/**
	 * @var array [object_id] => rating
	 */
	protected $known_data = array();

	/**
	 *
	 * @var array [i] => i-th Preference
	 */
	protected $preferences = array();

	/**
	 * 
	 * @param array $training_data [object_id] => rating
	 * @param Objects $objects
	 */
	function __construct(array $training_data, $objects) {

		// $preferences[preference_index][i] => {[0] => value, [1] => rating}
		$preferences = array();

		foreach ($objects->getColumnNames() as $preference_index => $preference_name) {
			$preferences[$preference_index] = array();
		}

		// Learn preferences
		foreach ($training_data as $pair) {
			$object_id = $pair[0];
			$rating = $pair[1];

			$this->known_data[$object_id] = $rating;

			foreach ($objects->Get($object_id) as $preference_index => $value) {
				$preferences[$preference_index][] = array($value, $rating);
			}
		}

		$string_columns = array('vyrobce', 'model', 'palivo');
		foreach ($preferences as $preference_index => $rated_values) {
			if (in_array($objects->getColumnNames()[$preference_index], $string_columns)) {
				$this->preferences[] = new StringPreference($rated_values);
			} else {
				$this->preferences[] = new NumericPreference($rated_values);
			}
		}
	}

	public function Rate($object_id, Objects $objects) {
		if (isset($this->known_data[$object_id])) {
			// Already known from training
			return $this->known_data[$object_id];
		} else {
			// Predict rating using all attributes and compute weighted average
			$object = $objects->Get($object_id);

			$result = 0;
			$total_weight = 0;

			foreach ($this->preferences as $preference_id => $preference) {
				$weight = $preference->getWeight();
				$result += $preference->getRating($object[$preference_id]) * $weight;
				$total_weight += $weight;
			}

			$result /= $total_weight;

			return $result;
		}
	}

}

new Recommender("train100.csv", "test100.csv", "dummy_solution100.csv", "items.csv");
