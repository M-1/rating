<?php

function stats($file) {

	$set = new NumberSet();

	$handle = fopen($file, 'r');
	while (($buffer = fgets($handle)) !== false) {
		// $row[0] = id, $row[1] = vyrobce, $row[2] = model
		$row = explode(',', trim($buffer));

		$set->add($row[2]);
	}

	echo 'var: ' . $set->getVariance() . "\n";
	echo 'avg: ' . $set->getAverage() . "\n";
	echo 'med: ' . $set->getMedian() . "\n";
	echo 'count: ' . $set->getCount() . "\n";
}

class NumberSet {

	protected $data = array();
	protected $finalized = FALSE;

	public function add($value) {
		$this->data[] = $value;
	}

	public function finalize() {
		if (!$this->finalized) {
			sort($this->data);
			$this->finalized = TRUE;
		}
	}

	public function getCount() {
		return count($this->data);
	}
	
	protected function getIndex($position) {
		return (int) ($position * (count($this->data) - 1));
	}

	public function getMedian() {
		return $this->getPercentile(0.5);
	}

	/**
	 * 
	 * @param float $value in interval [0, 1]
	 * @return type
	 */
	public function getPercentile($value) {
		$this->finalize();
		return $this->data[$this->getIndex($value)];
	}

	/**
	 * 
	 * @author thearbitcouncil at gmail dot com
	 * @link http://php.net/manual/en/ref.math.php#48783 source
	 * @param type $arr
	 * @return int
	 */
	function getAverage() {
		if (!count($this->data)) {
			return 0;
		}

		$sum = 0;
		for ($i = 0; $i < count($this->data); $i++) {
			$sum += $this->data[$i];
		}

		return $sum / count($this->data);
	}

	/**
	 * 
	 * @author thearbitcouncil at gmail dot com
	 * @link http://php.net/manual/en/ref.math.php#48783 source
	 * @param type $arr
	 * @return int
	 */
	public function getVariance() {
		if (!count($this->data)) {
			return 0;
		}

		$mean = $this->getAverage();

		$sos = 0; // Sum of squares
		for ($i = 0; $i < count($this->data); $i++) {
			$sos += ($this->data[$i] - $mean) * ($this->data[$i] - $mean);
		}

		return $sos / (count($this->data) - 1);  // denominator = n-1; i.e. estimating based on sample 
		// n-1 is also what MS Excel takes by default in the
		// VAR function
	}

}

stats($_SERVER['argv'][1]);